const https = require("https");

module.exports = async function disqus(link, options) {
  // Get the link and api key from the options
  let api_key = options.hash.api_key;
  let forum = options.hash.forum;
  let response = options.hash.response || "comments";

  // Usando una promesa
  async function getHttps(url) {
    let promesa = new Promise((resolve, reject) => {
      https
        .get(url, (res) => {
          let data = "";
          // A chunk of data has been received.
          res.on("data", (chunk) => {
            data += chunk;
          });
          // The whole response has been received. Resolve the promise with the JSON data.
          res.on("end", () => {
            let response = JSON.parse(data);
            resolve(response);
          });
        })
        .on("error", (err) => {
          // Reject the promise with the error.
          reject(err);
        });
    });
    // Esperar a que la promesa se resuelva y retornar el valor
    let valor = await promesa;
    return valor;
  }

  const threadUrl = `https://disqus.com/api/3.0/threads/details.json?api_key=${api_key}&thread:link=${link}&forum=${forum}`;
  const comments = await getHttps(threadUrl);
  const { id, posts } = comments.response;

  if (response === "comments") {
    return posts;
  } else {
    const votesUrl = `https://disqus.com/api/3.0/threadReactions/loadReactions.json?api_key=${api_key}&thread=${id}`;
    const votes = await getHttps(votesUrl);
    const { reactions } = votes.response;

    //0=upvote, 1=funny, 2=love, 3=surprised, 4=angry, 5=sad, default=comments
    return response === "upvote"
      ? reactions[0].votes
      : response === "funny"
      ? reactions[1].votes
      : response === "love"
      ? reactions[2].votes
      : response === "surprised"
      ? reactions[3].votes
      : response === "angry"
      ? reactions[4].votes
      : response === "sad"
      ? reactions[5].votes
      : posts;
  }
};

module.exports.async = true;
