// Histoire
import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";
import UnoCSS from "@unocss/svelte-scoped/vite";
// import transformerDirectives from "@unocss/transformer-directives";

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    // target: "esnext",
    outDir: "custom_elements",
    minify: true,
    sourcemap: false,
    rollupOptions: {
      input: "src/lib/index.ts",
      // input: {
      //   Counter: "src/lib/Counter.svelte",
      //   Logo: "src/lib/Logo/Logo.svelte",
      //   Menu: "src/lib/Menu/Menu.svelte",
      //   MenuItem: "src/lib/Menu/MenuItem.svelte",
      // },
      output: {
        entryFileNames: "[name].js",
        chunkFileNames: "[name]-[hash].js",
        // chunkFileNames: "[name].js",
        assetFileNames: "[name].[ext]",
        format: "esm",
      },
    },
  },
  plugins: [
    UnoCSS({
      // injectReset: "@unocss/reset/tailwind.css",
      // cssFileTransformers: [transformerDirectives()],
    }),
    svelte({
      compilerOptions: {
        customElement: true,
      },
    }),
  ],
});
