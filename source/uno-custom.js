export default {
  theme: {
    fontSize: {
      "2xs": ["12px", "145%"],
      xs: ["14px", "140%"],
      sm: ["16px", "135%"],
      base: ["18px", "130%"],
      lg: ["20px", "125%"],
      xl: ["24px", "120%"],
      "2xl": ["28px", "115%"],
      "3xl": ["32px", "110%"],
      "4xl": ["36px", "105%"],
      "5xl": ["40px", "100%"],
    },
  },
  shortcuts: {
    "left-x-divider":
      "before:content-[''] before:absolute before:h-full before:left-0 before:top-0 before:-ml-6 before:border-l before:border-neutral-200",
    "right-x-divider":
      "after:content-[''] after:absolute after:h-full after:right-0 after:top-0 after:-mr-6 after:border-r after:border-neutral-200",
    "top-y-divider":
      "before:content-[''] before:absolute before:w-full before:left-0 before:top-0 before:-mt-6 before:border-t before:border-neutral-200",
    "bottom-y-divider":
      "after:content-[''] after:absolute after:w-full after:bottom-0 after:left-0 after:-mb-6 after:border-b after:border-neutral-200",
    "no-left-x-divider": "before:border-l-0",
    "no-right-x-divider": "after:border-r-0",
    "no-top-y-divider": "before:border-t-0",
    "no-bottom-y-divider": "after:border-b-0",
    "author-link":
      "font-400 text-2xs font-sans tracking-wide text-neutral-700 no-underline uppercase",
    "author-bullet": "i-ri-arrow-right-double-line block h-4 w-4 bg-red-500",
    "btn-primary":
      "py-2 px-4 rounded bg-red-500 text-xs text-red-100 font-sans font-700 tracking-wide",
    section:
      "sm:max-w-640px md:max-w-768px lg:max-w-1024px xl:max-w-1280px mx-auto p-4",
    "headline-1": "font-sans text-4xl md:text-5xl",
    "headline-2": "font-sans text-3xl md:text-4xl",
    "headline-3": "font-sans text-2xl md:text-3xl",
    "headline-4": "font-sans text-xl md:text-2xl",
    "headline-5": "font-sans text-lg md:text-xl",
    "headline-6": "font-sans text-base md:text-lg",
    "post-meta": "font-sans text-2xs text-neutral-500",
    "post-content":
      "text-base md:text-lg text-neutral-800 leading-7 md:leading-8",
  },
  fonts: {
    sans: "Excon:400,500,700",
    serif: "Neco:400,401,700,701",
  },
  content: {
    pipeline: {
      include: [/\.(svelte|hbs|js|ts|html)($|\?)/],
      exclude: ["src/lib/**/*.{svelte,js,ts}"],
    },
    filesystem: ["./../**/*.hbs"],
  },
};
