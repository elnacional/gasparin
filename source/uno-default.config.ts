// Svelte
import { defineConfig, presetUno, presetIcons, presetWebFonts } from "unocss";
import transformerDirectives from "@unocss/transformer-directives";
import extractorSvelte from "@unocss/extractor-svelte";
import customConfig from "./uno-custom.js";

export default defineConfig({
  theme: customConfig.theme,
  shortcuts: customConfig.shortcuts,
  presets: [
    presetUno(),
    presetIcons(),
    presetWebFonts({
      provider: "fontshare",
      fonts: customConfig.fonts,
    }),
  ],
  extractors: [extractorSvelte()],
  transformers: [transformerDirectives()],
  content: {
    pipeline: {
      include: [/\.(svelte|hbs|js|ts|html)($|\?)/],
      exclude: ["src/lib/**/*.{svelte,js,ts}"],
    },
    filesystem: ["./../**/*.hbs"],
  },
});
