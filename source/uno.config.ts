// Histoire
import { defineConfig, presetUno, presetIcons, presetWebFonts } from "unocss";
import customConfig from "./uno-custom.js";

export default defineConfig({
  theme: customConfig.theme,
  shortcuts: customConfig.shortcuts,
  presets: [
    presetUno(),
    presetIcons(),
    presetWebFonts({
      provider: "fontshare",
      fonts: customConfig.fonts,
    }),
  ],
});
