# Introduccion

![Screenshot](./assets/screenshot.jpg)

Este tema esta creado usando usando [Svelte](https://svelte.dev/) y [UnoCSS](https://unocss.dev/), este ultimo de integra mediante [Svelte Scoped](https://unocss.dev/integrations/svelte-scoped) para incluir las clases css en web component del tipo [Custom elements](https://svelte.dev/docs/custom-elements-api)

Para probar los componentes de una manera aislada se usa [Histoire](https://histoire.dev/), donde crean historias, que es un escenario en el que se muestran en el navegador uno o varios componentes para casos de uso específicos.

# Instalación

```
git clone https://gitlab.com/elnacional/gasparin.git
```

## Modulos para el tema de Ghost CMS

```
cd gasparin
pnpm i
```

## Modulos para los web components

```
cd gasparin/source
pnpm i
```

Es necesario ejecutar ambos para los siguientes comandos

## Crear los web components y clases utilitarias

En el directorio / ejecutar

```
pnpm build // crea los archivos css/bundle y js/bundle.js
pnpm watch
```

## Probar los web components

En el directorio /source ejecutar

```
pnpm story:dev // probar en local http://127.0.0.1:6006
pnpm story:build
pnpm story:preview
```

## Helpers personalizados para el tema

Actualmente [no hay forma](https://forum.ghost.org/t/custom-theme-hbs-helper/4038) de agregar helpers personalizados al tema del tipo `helpers/my_helper.js`, asi que se hizo de la [manera que se sugiere](https://forum.ghost.org/t/handlebars-helper/13821/4) en los foros usando un enlace simbolico al helper al directorio `current/core/frontend/helpers/my_helper.js`.

```
ln -s /dir_ghost_install/content/themes/gasparin/helpers/disqus.js /dir_ghost_install/current/core/frontend/helpers/disqus.js
```
